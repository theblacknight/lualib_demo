CXX = g++
LIB_DIR = Data/lib
LUA_DIR = ${LIB_DIR}/lua-5.2.1/src
SDL = -framework SDL2
OPENGL = -framework OPENGL
LUA = -L$(LUA_DIR) -llua
CXXFLAGS = -Wall -c -std=c++11 -I ~/Library/Frameworks/SDL2.framework/Headers -I /usr/include -I $(LUA_DIR)
LDFLAGS = -v $(OPENGL) $(SDL) -F /Library/Frameworks $(LUA)
EXE = main
SRC_DIR=Source/src/
BUILD_DIR=Build/

all: $(EXE)

$(EXE): main.o
	$(CXX) $(LDFLAGS) Build/$< -o $(BUILD_DIR)$@

main.o: $(SRC_DIR)main.cpp
	$(CXX) $(CXXFLAGS) $< -o $(BUILD_DIR)$@

clean:
	rm $(BUILD_DIR)*.o && rm $(BUILD_DIR)$(EXE)