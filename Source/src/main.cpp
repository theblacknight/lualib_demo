#include <iostream>
#include <unistd.h>
#include <SDL2/SDL.h>
#include <ctime>

#include <OpenGL/GLU.h>

#ifdef __cplusplus
extern "C" {
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}
#endif

using namespace std;

#define PROGRAM_NAME "Test OpenGL SDL"
const int screen_size[2] = {800,600};
float fill_color[4] = {1.0f, 1.0f, 1.0f, 1.0f};

void sdldie(const char *msg)
{
    printf("%s: %s\n", msg, SDL_GetError());
    SDL_Quit();
    exit(1);
}

void checkSDLError(int line = -1)
{
#ifndef NDEBUG
    const char *error = SDL_GetError();
    if (*error != '\0')
    {
        printf("SDL Error: %s\n", error);
        if (line != -1)
            printf(" + line: %i\n", line);
        SDL_ClearError();
    }
#endif
}

bool get_input(void) 
{
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT: return false; //The little X in the window got pressed
            case SDL_KEYDOWN:
                if (event.key.keysym.sym==SDLK_ESCAPE) {
                    return false;
                }
                break;
        }
    }
    return true;
}

SDL_Window* sdl_setup() 
{
    cout<<"Application starting."<<endl;
    cout<<"Initialising SDL."<<endl;
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
        sdldie("Unable to initialize SDL");

    SDL_Window *win = SDL_CreateWindow(PROGRAM_NAME, 
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 
        640, 480, 
        SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);

    if (win == nullptr){
        sdldie("SDL_CreateWindow Error.");
    }
    checkSDLError(__LINE__);

    return win;
}

SDL_GLContext gl_setup(SDL_Window* win) 
{
    SDL_GLContext glContext; 
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 32);
    glContext = SDL_GL_CreateContext(win);
    checkSDLError(__LINE__);
    SDL_GL_SetSwapInterval(1);

    glEnable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);  
    return glContext;   
}

void exit(SDL_Window* win, SDL_GLContext glContext, lua_State* L) {
    cout<<"Cleaning SDL."<<endl;
    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(win);
    SDL_Quit();    
    cout<<"Cleaning up Lua state"<<endl;
    lua_close(L);
    cout<<"Application exiting."<<endl;
}

void draw_gl() 
{
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
 
    glViewport(0,0,screen_size[0],screen_size[1]);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0, (double)(screen_size[0])/(double)(screen_size[1]), 0.1f,100.0f);
    glMatrixMode(GL_MODELVIEW);
 
    glLoadIdentity();
    gluLookAt(0.0,0.0,10.0, 0.0,0.0,0.0, 0.0,1.0,0.0);
}

static int set_fill(lua_State *L)
{
    float r = luaL_checknumber(L, 1); 
    float g = luaL_checknumber(L, 2); 
    float b = luaL_checknumber(L, 3); 
    float a = luaL_checknumber(L, 4); 
    fill_color[0] = r;
    fill_color[1] = g;
    fill_color[2] = b;
    fill_color[3] = a;
    return 0;
}

static int draw_triangle(lua_State *L) 
{
    float x = luaL_checknumber(L, 1); 
    float y = luaL_checknumber(L, 2); 
    float z = luaL_checknumber(L, 3); 
    float rot = luaL_checknumber(L, 4); 
    float rotX = luaL_checknumber(L, 5); 
    float rotY = luaL_checknumber(L, 6); 
    float rotZ = luaL_checknumber(L, 7); 

    glPushMatrix();
    glTranslatef(x, y, z);
    glRotatef(rot, rotX, rotY, rotZ);
    glBegin(GL_TRIANGLES);
    glColor4f(fill_color[0], fill_color[1], fill_color[2], fill_color[3]);
    glVertex3f( 0.0f, 1.0f, 0.0f); 
    glColor4f(fill_color[0], fill_color[1], fill_color[2], fill_color[3]);
    glVertex3f( 1.0f, -1.0f, 0.0f);
    glColor4f(fill_color[0], fill_color[1], fill_color[2], fill_color[3]);
    glVertex3f( -1.0f, -1.0f, 0.0f); 
    glEnd();
    glPopMatrix();

    return 0;
}

int main(int argc, char** argv)
{
    SDL_Window* win = sdl_setup();    
    SDL_GLContext glContext = gl_setup(win);

    lua_State *L = luaL_newstate();
    luaL_openlibs(L);

    int errorCode = 0;
    if ((errorCode=luaL_loadfile(L, "script.lua"))) 
    {
        cout<<"Error occurred reading Lua script: "<<lua_tostring(L, -1)<<endl;
    }

    if ((errorCode = lua_pcall(L, 0, 0, 0))) 
    {
        cout<<"Error occurred priming Lua: "<<lua_tostring(L, -1)<<endl;
    }

    lua_pushcfunction(L, draw_triangle);
    lua_setglobal(L, "drawTriangle");
    lua_pushcfunction(L, set_fill);
    lua_setglobal(L, "setFillColor");

    glEnable(GL_BLEND);     // Turn Blending On
    glDisable(GL_DEPTH_TEST);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE); 
    glClearColor ( 0.0, 0.0, 0.0, 1.0 );

    clock_t t1, t2;
    t1 = t2 = clock();
    double elapsed = 0;

    while (true) {
        if (!get_input()) break;
        draw_gl();
        lua_getglobal(L, "draw");
        lua_pushnumber(L, elapsed);
        if((errorCode = lua_pcall(L, 1, 0, 0)))
        {
            cout<<"Error calling Lua draw function: "<<lua_tostring(L, -1)<<endl;
            break;
        }
        SDL_GL_SwapWindow(win);
        t2 = clock();
        elapsed = (double)(t2 - t1) / CLOCKS_PER_SEC * 1000;
        cout<<elapsed<<endl;
    }
 
    exit(win, glContext, L);
    return 0;
}