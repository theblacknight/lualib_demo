local rot = 0

function draw ( elapsed )
    setFillColor( 0.0, 1.0, 0.0, 1.0 )
    drawTriangle(-3, 0, 0, 45, 0, 1, 0) 
    drawTriangle(0, 0, 0, elapsed, 0, 0, 1)

    setFillColor( 1.0, 0.0, 0.0, 1.0 )
    drawTriangle(0, -3, 0, rot, 1, 0, 0) 
    drawTriangle(-3, -3, 0, rot, 1, 1, 1) 
    
    rot = rot + 5
end